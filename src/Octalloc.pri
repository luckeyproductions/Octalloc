HEADERS += \
    $$PWD/luckey.h \
    $$PWD/mastercontrol.h \
    $$PWD/sceneobject.h \
    $$PWD/spawnmaster.h \
    $$PWD/inputmaster.h \
    $$PWD/player.h \
    $$PWD/controllable.h \
    $$PWD/effectmaster.h \
    $$PWD/head.h \
    $$PWD/lantern.h \
    $$PWD/networkmaster.h \
    $$PWD/realm.h \
    $$PWD/barrel.h

SOURCES += \
    $$PWD/luckey.cpp \
    $$PWD/mastercontrol.cpp \
    $$PWD/sceneobject.cpp \
    $$PWD/spawnmaster.cpp \
    $$PWD/inputmaster.cpp \
    $$PWD/player.cpp \
    $$PWD/controllable.cpp \
    $$PWD/effectmaster.cpp \
    $$PWD/head.cpp \
    $$PWD/lantern.cpp \
    $$PWD/networkmaster.cpp \
    $$PWD/realm.cpp \
    $$PWD/barrel.cpp
